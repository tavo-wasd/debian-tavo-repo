# My personal dwmblocks build

## Description
My personal repository for a custom dwmblocks build, depends on scripts available in my dotfiles repo. 

## Dependencies
adobe-source-code-pro-fonts,
make,
libxcb
libX11-devel,
libXft-devel,
libXinerama-devel,

## Acknowledgement
@torrinfail for their dwmblocks build
