#!/bin/sh

printf '\n\033[1m    %b\033[0m%s\n\n' \
    "Building packages..." && sleep 1

# Build for each directory
for build in * ; do
    [ -d "$build" ] && dpkg --build ./"$build"
done

printf '\n\033[1m    %b\033[0m%s\n\n' \
    "Moving all .deb's into ../../pool/main" && sleep 1

# Move built packages
mv -v *.deb ../../pool/main/

printf '\n\033[1m    %b\033[0m%s\n\n' \
    "Done!"
