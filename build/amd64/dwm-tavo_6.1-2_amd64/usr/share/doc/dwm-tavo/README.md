# My personal dwm build

## Description
My personal repository for a custom dwm build currently using the Gruvbox theme.

## Patches applied to suckless' software
Applied via \*.diff files found in suckless.org, in the appearing order.
Unsuccessful patches generated a \*.rej file which was moved to the corresponding "patches/manpatch/" directory for each program, and renamed \*.manpatch. This was then used to manually patch the source code.

### dwm
(2147 lines of code unpatched, added +424 lines for 2571 total)
- swallow
- pertag
- fullgaps
- movestack
- notitle
- attachbottom
- warp
- alwayscenter
- sticky

## Dependencies (Archlinux names)
ttf-nerd-fonts-symbols-2048-em-mono.
make,
libxcb
libX11-devel,
libXft-devel,
libXinerama-devel,
alsa-utils,
cilpmenu,
scrot,
dunst,

## Default apps
librewolf,
mpv,
vim,
nnn,

## Not necessary but useful in laptops
iwd,
brightnessctl,
