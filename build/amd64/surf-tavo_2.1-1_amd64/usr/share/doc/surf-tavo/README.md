# My personal surf build

## Description
My personal repository for a custom surf build currently using the Gruvbox theme.

## Patches applied to suckless' software
Applied via \*.diff files found in suckless.org, in the appearing order.
Unsuccessful patches generated a \*.rej file which was moved to the corresponding "patches/manpatch/" directory for each program, and renamed \*.manpatch. This was then used to manually patch the source code.

### surf
- homepage
- websearch

## Dependencies (Archlinux names)
webkit2gtk, 
make,
libxcb
libX11-devel,
libXft-devel,
libXinerama-devel,
