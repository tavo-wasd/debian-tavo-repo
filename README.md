# debian-tavo

Personal debian repository for convenience!

To install the public GPG key:

```sh
sudo curl -Lo /etc/apt/trusted.gpg.d/tavo@tavo.one.asc pubkey.tavo.one
```

Add the repository to `/etc/apt/sources.list.d/debian-tavo.list:`

```sh
echo "deb [arch=amd64,all] https://gitlab.com/tavo-wasd/debian-tavo/-/raw/main stable main" | sudo tee /etc/apt/sources.list.d/debian-tavo.list
```

Finally, update the package index
```sh
sudo apt update
```

To uninstall the repo:

```sh
sudo rm /etc/apt/trusted.gpg.d/tavo@tavo.one.asc
sudo rm /etc/apt/sources.list.d/debian-tavo.list
sudo apt update
```
