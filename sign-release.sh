#!/bin/sh

cat dists/stable/Release | gpg --default-key tavo@tavo.one -abs > dists/stable/Release.gpg
cat dists/stable/Release | gpg --default-key tavo@tavo.one -abs --clearsign > dists/stable/InRelease
